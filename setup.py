from setuptools import setup, find_packages

setup(
    name='blueorange',
    version='1.4.0',
    description='Libreria de procesos genericos',
    long_description='Libreria de procesos genericos',
    url='',
    author='BLUEORANGE GROUP',
    author_email='lvalentinetti@blueorange.com.ar',
    license='',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='Libreria de procesos genericos',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['six', 'xlwt'],
    extras_require={},
    package_data={},
    data_files=[],
    entry_points={},
)
