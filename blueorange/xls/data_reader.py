# -*- encoding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


class DataReader():
    """ Clase que toma una hoja de Excel y lee los contenidos a una lista de listas """
    def __init__(self, height, list_colums_to_jump):
        self.header_height = height  # Altura del encabezado (para excluirlo de la lectura)
        self.columns_to_jump = list_colums_to_jump  # Columnas a saltear al leer xls (para excluirlo de la lectura)

    def read_data(self, sheet):
        rows = []
        for r in range(sheet.nrows - self.header_height):
            cols = []
            for c in range(sheet.ncols):
                # Saltea columnas del excel
                if c in self.columns_to_jump:
                    continue
                val = sheet.cell(r + self.header_height, c).value
                if type(val) == float and val.is_integer():
                    val = int(val)
                cols.append(val)
            rows.append(cols)
        return rows

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
