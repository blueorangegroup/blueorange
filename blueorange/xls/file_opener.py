# -*- encoding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlrd, base64


class FileOpener():
    """ Clase que abre un archivo Excel traído desde Odoo y devuelve la primera hoja """
    @staticmethod
    def open_file(file):
        try:
            file_path = "/tmp/file.xlsx"
            tmp_file = open(file_path, "wb")
            tmp_file.write(base64.b64decode(file))
            tmp_file.close()
            return xlrd.open_workbook(file_path).sheet_by_index(0)
        except:
            return False

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
