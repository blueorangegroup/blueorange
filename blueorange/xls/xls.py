# -*- encoding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlwt, io, base64
from six import string_types

def generate_hyperlink(address, text):
    return xlwt.Formula('HYPERLINK("{}"; "{}")'.format(address, text))


class XlsBuilder:
    def __init__(self, sheet_name, styles={}):
        """
        Inicializa el builder para su uso
        :param sheet_name: nombre de la hoja del libro
        :param styles: diccionario de estilos a utilizar en el libro (nombre y especificacion)
        """
        self.book = xlwt.Workbook(encoding='utf-8')
        if sheet_name:
            self.add_sheet(sheet_name)
        self.styles = {}
        for style_name, style_detail in styles.items():
            if len(style_detail) == 1:
                self.styles[style_name] = xlwt.easyxf(style_detail[0])
            else:
                self.styles[style_name] = xlwt.easyxf(style_detail[0], num_format_str=style_detail[1])

    def add_sheet(self, sheet_name):
        self.sheet = self.book.add_sheet(sheet_name)

    def set_col_visibility(self, col, visible):
        self.sheet.col(col - 1).hidden = int(not visible)

    def merge_cells(self, row_from, row_to, col_from, col_to, name_cell, style):
        """
        Unifico varias celdas
        :param row_from: Numero de desde que fila
        :param row_to: Numero de hasta que fila
        :param col_from: Numero de desde que columna
        :param col_to: Numero de hasta que columna
        :param style: Estilo a utilizar
        """
        self.sheet.write_merge(row_from - 1, row_to - 1, col_from - 1, col_to - 1, name_cell, self.styles.get(style))

    def write_cell(self, row, col, data, style):
        """
        Escribe una celda en el archivo
        :param row: numero de fila de la celda a escribir (la primera es la 1)
        :param col: numero de columna de la celda a escribir (la primera es la 1)
        :param data: dato a escribir (si empieza con = se lo analiza como formula)
        :param style: estilo a utilizar
        """
        self.sheet.write(row - 1, col - 1,
                         xlwt.Formula(data[1:]) if isinstance(data, string_types) and data.startswith('=') else data,
                         self.styles.get(style))

    def write_row(self, row, data, styles, col=0):
        """
        Escribe una fila entera comenzando por la primera columna
        :param row: numero de la fila a escribir (la primera es la 1)
        :param data: iterable de datos que se escribiran
        :param styles: estilos a utilizar (1 para toda la fila o 1 para cada celda)
        :param col: numero de la columna inicial
        """
        for i in range(len(data)):
            self.write_cell(row, i + 1 + col, data[i], styles[0] if len(styles) == 1 else styles[i])

    def get_file(self):
        """
        Genera el archivo con los datos agregados hasta el momento
        :return: archivo encodeado
        """
        file_data = io.BytesIO()
        self.book.save(file_data)
        file = base64.b64encode(file_data.getvalue())
        return file

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
